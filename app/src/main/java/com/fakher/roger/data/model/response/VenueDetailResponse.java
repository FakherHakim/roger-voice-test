package com.fakher.roger.data.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class VenueDetailResponse {

    @Expose
    @SerializedName("response")
    private Response response;
    @Expose
    @SerializedName("meta")
    private Meta meta;

    public Response getResponse() {
        return response;
    }

    public static class Response {
        @Expose
        @SerializedName("venue")
        private VenueDetail venue;

        public VenueDetail getVenueDetail() {
            return venue;
        }
    }

    public static class VenueDetail {
        @Expose
        @SerializedName("attributes")
        private Attributes attributes;
        @Expose
        @SerializedName("venueChains")
        private List<String> venuechains;
        @Expose
        @SerializedName("inbox")
        private Inbox inbox;
        @Expose
        @SerializedName("pageUpdates")
        private Pageupdates pageupdates;
        @Expose
        @SerializedName("listed")
        private Listed listed;
        @Expose
        @SerializedName("timeZone")
        private String timezone;
        @Expose
        @SerializedName("shortUrl")
        private String shorturl;
        @Expose
        @SerializedName("tips")
        private Tips tips;
        @Expose
        @SerializedName("createdAt")
        private int createdat;
        @Expose
        @SerializedName("hereNow")
        private Herenow herenow;
        @Expose
        @SerializedName("reasons")
        private Reasons reasons;
        @Expose
        @SerializedName("photos")
        private Photos photos;
        @Expose
        @SerializedName("specials")
        private Specials specials;
        @Expose
        @SerializedName("beenHere")
        private Beenhere beenhere;
        @Expose
        @SerializedName("ok")
        private boolean ok;
        @Expose
        @SerializedName("dislike")
        private boolean dislike;
        @Expose
        @SerializedName("likes")
        private Likes likes;
        @Expose
        @SerializedName("stats")
        private Stats stats;
        @Expose
        @SerializedName("verified")
        private boolean verified;
        @Expose
        @SerializedName("categories")
        private List<String> categories;
        @Expose
        @SerializedName("canonicalUrl")
        private String canonicalurl;
        @Expose
        @SerializedName("location")
        private Location location;
        @Expose
        @SerializedName("contact")
        private Contact contact;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;
        private String url;
        private double rating;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getUrl() {
            return canonicalurl;
        }

        public double getRating() {
            return rating;
        }

        public Location getLocation() {
            return location;
        }
    }

    public static class Attributes {
        @Expose
        @SerializedName("groups")
        private List<String> groups;
    }

    public static class Inbox {
        @Expose
        @SerializedName("items")
        private List<String> items;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Pageupdates {
        @Expose
        @SerializedName("items")
        private List<String> items;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Listed {
        @Expose
        @SerializedName("groups")
        private List<Groups> groups;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Groups {
        @Expose
        @SerializedName("items")
        private List<String> items;
        @Expose
        @SerializedName("count")
        private int count;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("type")
        private String type;
    }

    public static class Tips {
        @Expose
        @SerializedName("groups")
        private List<Groups> groups;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Herenow {
        @Expose
        @SerializedName("groups")
        private List<String> groups;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Reasons {
        @Expose
        @SerializedName("items")
        private List<String> items;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Photos {
        @Expose
        @SerializedName("groups")
        private List<String> groups;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Specials {
        @Expose
        @SerializedName("items")
        private List<String> items;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Beenhere {
        @Expose
        @SerializedName("lastCheckinExpiredAt")
        private int lastcheckinexpiredat;
        @Expose
        @SerializedName("marked")
        private boolean marked;
        @Expose
        @SerializedName("unconfirmedCount")
        private int unconfirmedcount;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Likes {
        @Expose
        @SerializedName("groups")
        private List<String> groups;
        @Expose
        @SerializedName("count")
        private int count;
    }

    public static class Stats {
        @Expose
        @SerializedName("visitsCount")
        private int visitscount;
        @Expose
        @SerializedName("checkinsCount")
        private int checkinscount;
        @Expose
        @SerializedName("usersCount")
        private int userscount;
        @Expose
        @SerializedName("tipCount")
        private int tipcount;
    }

    public static class Location {
        @Expose
        @SerializedName("formattedAddress")
        private List<String> formattedaddress;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("cc")
        private String cc;
        @Expose
        @SerializedName("lng")
        private double lng;
        @Expose
        @SerializedName("lat")
        private double lat;
        @Expose
        @SerializedName("address")
        private String address;

        public String getAddress() {
            return address;
        }
    }

    public static class Contact {
    }

    public static class Meta {
        @Expose
        @SerializedName("requestId")
        private String requestid;
        @Expose
        @SerializedName("code")
        private int code;
    }
}
