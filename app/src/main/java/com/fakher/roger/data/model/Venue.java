package com.fakher.roger.data.model;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class Venue {

    private String mID;
    private String mName;
    private String mAddress;
    private String mURL;
    private double mLong;
    private double mLat;
    private double mRating;

    public Venue(String id, String name, String address, String url, double lng,
                 double lat, double rating) {

        this.mID = id;
        this.mName = name;
        this.mAddress = address;
        this.mURL = url;
        this.mLong = lng;
        this.mLat = lat;
        this.mRating = rating;
    }

    public Venue(String id, String name, String url, String address, double rating) {
        mID = id;
        mName = name;
        mURL = url;
        mAddress = address;
        mRating = rating;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "mID='" + mID + '\'' +
                ", mRating=" + mRating +
                '}' + "\n";
    }

    public String getID() {
        return mID;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String mURL) {
        this.mURL = mURL;
    }

    public double getLong() {
        return mLong;
    }

    public void setLong(double mLong) {
        this.mLong = mLong;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double mLat) {
        this.mLat = mLat;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double mRating) {
        this.mRating = mRating;
    }
}
