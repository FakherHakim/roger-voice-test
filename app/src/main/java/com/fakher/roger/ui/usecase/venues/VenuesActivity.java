package com.fakher.roger.ui.usecase.venues;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fakher.roger.R;
import com.fakher.roger.data.model.Venue;
import com.fakher.roger.ui.adapter.VenueAdapterListener;
import com.fakher.roger.ui.adapter.VenuesAdapter;

import java.util.Arrays;
import java.util.List;

import timber.log.Timber;

public class VenuesActivity extends AppCompatActivity implements VenuesContract.VenuesView {

    private static final int LOCATION_PERMISSIONS_REQUEST = 1;
    RecyclerView mVenuesRV;
    ProgressBar mProgressBar;
    TextView mNoVenuesView;
    private VenuesContract.Presenter mPresenter;
    private VenuesAdapter mVenuesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configureRecyclerView();
        mProgressBar = findViewById(R.id.loading_venues_progress_bar);
        mNoVenuesView = findViewById(R.id.no_venues_display);
        mPresenter = new VenuesPresenter(this);
        mPresenter.setView(this);

        getPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setLoadingIndicatorVisible(boolean active) {
        mProgressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showVenues(List<Venue> venues) {
        Timber.i("Response %s", venues.toString());
        mVenuesRV.setVisibility(View.VISIBLE);
        mNoVenuesView.setVisibility(View.GONE);
        mVenuesAdapter.setItems(venues);
        VenueAdapterListener.addTo(mVenuesRV)
                .setOnItemClickListener((recyclerView, position, v) -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(venues.get(position).getURL()));
                    startActivity(intent);
                });
    }

    @Override
    public void showLoadVenuesError(String message) {
        Timber.e("Error while loading venues %s", message);
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.dialog_error_title)
                //FIXME improve text by getting error code and telling what's wrong.
                .setMessage(R.string.dialog_body_text)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void showNoVenues() {
        mVenuesRV.setVisibility(View.GONE);
        mNoVenuesView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        if (requestCode == LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length != 2 ||
                    grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[1] != PackageManager.PERMISSION_GRANTED) {

                boolean showRationale = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    showRationale = shouldShowRequestPermissionRationale(
                            Arrays.toString(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION}));
                }

                if (!showRationale) {
                    Toast.makeText(this, "Location permissions denied", Toast.LENGTH_SHORT).show();
                    setLoadingIndicatorVisible(false);
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void getPermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_PERMISSIONS_REQUEST);
            }
        }
    }

    private void configureRecyclerView() {
        mVenuesRV = findViewById(R.id.venues_recycler_view);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        mVenuesRV.addItemDecoration(itemDecorator);
        mVenuesAdapter = new VenuesAdapter();
        mVenuesRV.setAdapter(mVenuesAdapter);
        mVenuesRV.setLayoutManager(new LinearLayoutManager(this));
    }
}
