package com.fakher.roger.data;

import com.fakher.roger.BuildConfig;
import com.fakher.roger.data.model.Venue;
import com.fakher.roger.data.model.response.VenueDetailResponse;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class RemoteDataSource {

    private ApiService mApiService;

    public RemoteDataSource(ApiService apiService) {
        mApiService = apiService;
    }

    public Flowable<List<Venue>> getNearByVenues(String ll) {

        return mApiService.getVenues(ll, BuildConfig.API_TOKEN, "20180326")
                .flatMapIterable(venuesResponse -> venuesResponse.getResponse().getVenues())
                .flatMap(venues -> mApiService.getVenueDetail(venues.getId(),
                        BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, "20180326"))
                .map(detailResponse -> {
                    VenueDetailResponse.VenueDetail venuesDetail = detailResponse.getResponse().getVenueDetail();
                    String id = venuesDetail.getId();
                    String name = venuesDetail.getName();
                    String url = venuesDetail.getUrl();
                    String address = venuesDetail.getLocation().getAddress();
                    double rating = venuesDetail.getRating();
                    return new Venue(id, name, url, address, rating);
                })
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
