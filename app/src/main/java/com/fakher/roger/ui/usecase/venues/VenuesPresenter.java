package com.fakher.roger.ui.usecase.venues;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.fakher.roger.RogerApplication;
import com.fakher.roger.data.RemoteDataSource;
import com.fakher.roger.service.LocationService;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class VenuesPresenter implements VenuesContract.Presenter {

    @SuppressWarnings("WeakerAccess")
    @Inject
    RemoteDataSource mRemoteDataSource;

    private AppCompatActivity mActivity;
    private CompositeDisposable mCompositeDisposable;
    private BroadcastReceiver locationListenerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double longitude = intent.getDoubleExtra(LocationService.LONGITUDE, 0);
            double latitude = intent.getDoubleExtra(LocationService.LATITUDE, 0);
            loadVenues(longitude + "," + latitude);
        }
    };

    public VenuesPresenter(Context context) {

        RogerApplication.get(context)
                .getAppComponent()
                .inject(this);

        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void setView(VenuesContract.VenuesView view) {
        mActivity = (AppCompatActivity) view;
    }

    @Override
    public void subscribe() {

        //Start location service.
        Intent intent = new Intent(mActivity, LocationService.class);
        mActivity.startService(intent);

        //Register to location service broadcast
        IntentFilter filter = new IntentFilter(LocationService.ACTION);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(locationListenerReceiver, filter);
    }

    @Override
    public void unsubscribe() {

        //Stop location service.
        Intent intent = new Intent(mActivity, LocationService.class);
        mActivity.stopService(intent);

        //Unregister to location service broadcast
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(locationListenerReceiver);

        mCompositeDisposable.clear();
    }

    @Override
    public void loadVenues(String ll) {

        VenuesContract.VenuesView view = (VenuesContract.VenuesView) mActivity;

        view.setLoadingIndicatorVisible(true);

        Disposable disposable = mRemoteDataSource.getNearByVenues(ll)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(venues -> {
                            if (venues != null && !venues.isEmpty()) {
                                view.showVenues(venues);
                            } else {
                                view.showNoVenues();
                            }
                            view.setLoadingIndicatorVisible(false);
                        },
                        throwable -> {
                            view.showLoadVenuesError(throwable.getMessage());
                            view.setLoadingIndicatorVisible(false);
                        });

        mCompositeDisposable.add(disposable);
    }
}
