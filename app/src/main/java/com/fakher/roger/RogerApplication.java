package com.fakher.roger;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.fakher.roger.di.AppComponent;
import com.fakher.roger.di.AppModule;
import com.fakher.roger.di.DaggerAppComponent;
import com.fakher.roger.di.NetworkModule;
import com.fakher.roger.utils.LinkingDebugTree;

import timber.log.Timber;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class RogerApplication extends Application {

    private AppComponent mAppComponent;

    public static RogerApplication get(Context context) {
        return (RogerApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new LinkingDebugTree());
            Stetho.initializeWithDefaults(this);
        }

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule("https://api.foursquare.com/"))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
