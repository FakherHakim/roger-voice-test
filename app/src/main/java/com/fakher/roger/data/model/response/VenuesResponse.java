package com.fakher.roger.data.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fakhakim on 26/03/2018.
 */

public class VenuesResponse {

    @Expose
    @SerializedName("response")
    private Response mResponse;

    @Expose
    @SerializedName("meta")
    private Meta mMeta;

    public Response getResponse() {
        return mResponse;
    }

    public static class Venues {

        @Expose
        @SerializedName("id")
        private String id;

        public String getId() {
            return id;
        }
    }

    private static class Menu {
    }

    private static class Meta {
        @Expose
        @SerializedName("requestId")
        private String mRequestid;
        @Expose
        @SerializedName("code")
        private int mCode;
    }

    public class Response {

        @Expose
        @SerializedName("venues")
        private List<Venues> mVenues;

        public List<Venues> getVenues() {
            return mVenues;
        }
    }
}
