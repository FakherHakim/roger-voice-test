package com.fakher.roger.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import timber.log.Timber;

/**
 * Created by fakher on 29/03/18.
 */

public class LocationService extends Service {

    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    private static final String TAG = LocationService.class.getSimpleName();
    public static final String ACTION = TAG + "LocationAction";
    private static final int LOCATION_INTERVAL = 2500;
    private static final float LOCATION_DISTANCE = 100f;
    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    private LocationManager mLocationManager = null;
    private LocalBroadcastManager mLocalBroadcastManager;

    @Override
    public void onCreate() {

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Timber.e(ex, "fail to request location update, ignore");
        } catch (IllegalArgumentException ex) {
            Timber.e("network provider does not exist, %s", ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Timber.e(ex, "fail to request location update, ignore");
        } catch (IllegalArgumentException ex) {
            Timber.e("gps provider does not exist %s", ex.getMessage());
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        if (mLocationManager != null) {
            for (LocationListener mLocationListener : mLocationListeners) {
                try {
                    mLocationManager.removeUpdates(mLocationListener);
                } catch (Exception ex) {
                    Timber.e(ex, "fail to remove location listeners, ignore");
                }
            }
        }
        super.onDestroy();
    }

    private void initializeLocationManager() {

        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        private LocationListener(String provider) {

            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Timber.i("onLocationChanged: %s", location);
            mLastLocation.set(location);
            Intent intent = new Intent(ACTION);
            intent.putExtra(LONGITUDE, location.getLongitude());
            intent.putExtra(LATITUDE, location.getLatitude());
            mLocalBroadcastManager.sendBroadcast(intent);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Timber.i("onProviderDisabled: %s", provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Timber.i("onProviderEnabled: %s", provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Timber.i("onStatusChanged: %s", provider);
        }
    }
}
