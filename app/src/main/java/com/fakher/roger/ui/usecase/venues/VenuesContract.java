package com.fakher.roger.ui.usecase.venues;

import com.fakher.roger.data.model.Venue;

import java.util.List;

/**
 * Created by fakhakim on 26/03/2018.
 */

public interface VenuesContract {

    public interface VenuesView {

        void setLoadingIndicatorVisible(boolean active);

        void showVenues(List<Venue> venues);

        void showLoadVenuesError(String message);

        void showNoVenues();
    }

    public interface Presenter {

        public void setView(VenuesView view);

        public void subscribe();

        public void unsubscribe();

        public void loadVenues(String ll);
    }
}
