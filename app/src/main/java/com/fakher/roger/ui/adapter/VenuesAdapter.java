package com.fakher.roger.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fakher.roger.R;
import com.fakher.roger.data.model.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fakhakim on 30/03/2018.
 */

public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.ViewHolder> {

    private List<Venue> mVenues = new ArrayList<>();

    @Override
    public VenuesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View venueView = inflater.inflate(R.layout.item_venue_rv, parent, false);

        return new ViewHolder(venueView);
    }

    @Override
    public void onBindViewHolder(VenuesAdapter.ViewHolder holder, int position) {

        Venue venue = mVenues.get(position);

        //Set view title
        TextView name = holder.nameView;
        TextView rating = holder.ratingView;
        TextView address = holder.addressView;

        name.setText(venue.getName());
        rating.setText(String.valueOf(venue.getRating()));
        address.setText(venue.getAddress());
    }

    @Override
    public int getItemCount() {
        return mVenues.size();
    }

    public List<Venue> getItems() {
        return mVenues;
    }

    public void setItems(List<Venue> venues) {
        mVenues.clear();
        mVenues.addAll(venues);
        notifyDataSetChanged();
    }

    /**
     * Basic view holder class to be used in the recycler view.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameView;
        TextView ratingView;
        TextView addressView;

        ViewHolder(View itemView) {

            super(itemView);
            nameView = itemView.findViewById(R.id.venue_name);
            ratingView = itemView.findViewById(R.id.venue_rating);
            addressView = itemView.findViewById(R.id.venue_address);
        }
    }
}
