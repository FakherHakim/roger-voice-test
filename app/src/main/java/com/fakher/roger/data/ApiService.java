package com.fakher.roger.data;

import com.fakher.roger.data.model.response.VenueDetailResponse;
import com.fakher.roger.data.model.response.VenuesResponse;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by fakhakim on 26/03/2018.
 */

public interface ApiService {

    @GET("v2/venues/search")
    Flowable<VenuesResponse> getVenues(@Query("ll") String findBy,
                                       @Query("oauth_token") String token,
                                       @Query("v") String version);

    @GET("v2/venues/{venue_id}")
    Flowable<VenueDetailResponse> getVenueDetail(@Path("venue_id") String venueID,
                                                 @Query("client_id") String clientID,
                                                 @Query("client_secret") String clientSecret,
                                                 @Query("v") String version);
}
