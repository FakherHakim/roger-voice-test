package com.fakher.roger.di;

import com.fakher.roger.ui.usecase.venues.VenuesPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fakhakim on 26/03/2018.
 */

@Singleton
@Component(
        modules = {AppModule.class, NetworkModule.class}
)
public interface AppComponent {

    void inject(VenuesPresenter venuesVenuesPresenter);
}
